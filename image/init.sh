#!/bin/bash
set -e 

REMOTE_TOOL_REGISTRY_BRANCH=${REMOTE_TOOL_REGISTRY_BRANCH:-master}

initialize() {
    cd /
    git clone https://github.com/clarin-eric/switchboard-tool-registry.git
    cd /switchboard-tool-registry
    git fetch
    git checkout "${REMOTE_TOOL_REGISTRY_BRANCH}"
    chown -R "${TOOLS_USER}":"${TOOLS_USER}" /switchboard-tool-registry
}

if [ -d /switchboard-tool-registry/.git ]; then
    cd /switchboard-tool-registry
    /usr/local/bin/update-tool-registry.sh "$@"
else
    initialize "$@"
fi
exit
