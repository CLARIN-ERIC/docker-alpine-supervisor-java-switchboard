#!/bin/bash

set -e

echo "Checking remote git tool registry for updates..."
git fetch
HEADHASH=$(git rev-parse HEAD)
UPSTREAMHASH=$(git ls-remote origin -h "refs/heads/${REMOTE_TOOL_REGISTRY_BRANCH}" |cut -f1)

if [ "$HEADHASH" != "$UPSTREAMHASH" ]; then
    echo "New update available on tool registry (${REMOTE_TOOL_REGISTRY_BRANCH} branch). Fetching..."
    git checkout "${REMOTE_TOOL_REGISTRY_BRANCH}"
    git pull
else
	echo "Switchboard tool registry on branch \"${REMOTE_TOOL_REGISTRY_BRANCH}\" is up to date. Nothing to do!"
fi